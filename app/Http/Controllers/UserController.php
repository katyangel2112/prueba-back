<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Carbon;

use App\Models\User;
use App\Models\Profile;

class UserController extends Controller
{
    public function index()
    {
        $usuarios = User::whereNull('email')->with('profile')->orderBy('id','desc')->paginate(10);

        return $usuarios;
    }

    public function store(Request $request)
    {
    	Validator::make($request->all(), [
    		'first_name'        => 'required',
    		'last_name'         => 'required',
    		'description'       => 'required',
            'ima_profile'       => 'mimes:jpeg,png|file|max:1024'
        ], 
        $messages = 
        [
        	'first_name.required' => 'Ingrese el Nombre del Usuario',
        	'last_name.required'  => 'Ingrese el Apellido del Usuario',
        	'description.required'=> 'Ingrese el Descripción del Usuario',
            'ima_profile.file'    => 'Debe ser un Archivo',
            'ima_profile.max'     => 'Debe tener un maximo de 1024 KB',
            'ima_profile.mimes'   => 'Debe ser una imagen jpeg o png'
        ])->validate();

    	$user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'description' => $request->description
        ]);

        if($request->ima_profile != null) {
	        $folder = env('MEDIA_DISK');

            $imagen_v_path = "fotos/".$request->ima_profile; 
            Storage::disk($folder)->delete($imagen_v_path);

            $img = Image::make($request->ima_profile)->resize(200, 300)->encode('jpg', 80);
            $now = Carbon::now()->format('YmdHis');

            $imageName = $now.'.jpg';
             
            Storage::disk($folder)->put("fotos/{$imageName}", $img, 'public'); 

	        $profile = Profile::create([
	            'user_id' => $user->id,
	            'ima_profile' => env('STORAGE_URL').$imageName,
	        ]);

	    } else {
	    	$imageName = "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjE3Nzg0fQ";

	    	$profile = Profile::create([
	            'user_id' => $user->id,
	            'ima_profile' => env('STORAGE_URL').$imageName,
	        ]);
	    }

        return 200;
    }

    public function editar(Request $request)
    {
        $user = User::find($request->id);

        return $user;
    }

    public function update(Request $request){

    	Validator::make($request->all(), [
    		'first_name'        => 'required',
    		'last_name'         => 'required',
    		'description'       => 'required',
        ], 
        $messages = 
        [
        	'first_name.required' => 'Ingrese el Nombre del Usuario',
        	'last_name.required'  => 'Ingrese el Apellido del Usuario',
        	'description.required'=> 'Ingrese el Descripción del Usuario',
        ])->validate();

    	$user = User::where('id',$request->id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'description' => $request->description
        ]);

        return 200;
    }

    public function delete(Request $request)
    {
    	$user = User::find($request->id);
    	$user->delete();

    	return 200;
    }

    public function search_by_name(Request $request)
    {
    	$result = User::whereNull('email')
        ->where(function ($query) use ($request) {
           $query->where('first_name', 'LIKE', '%'.$request->b_nombre.'%')
            ->orWhere('last_name', 'LIKE', '%'.$request->b_nombre.'%');
        })
        ->with('profile')
        ->get();

    	return $result;
    }

    public function search_by_id(Request $request){
    	$result = User::whereNull('email')->where('id',$request->id)->with('profile')->get();

    	return $result;
    }

    public function search_by_date(Request $request)
    {
        $date = Carbon::parse($request->date)->format('Y-m-d');
    	$result = User::whereNull('email')
        ->whereDate('created_at', $date)
        ->orderBy('id','desc')
        ->with('profile')
        ->get();

    	return $result;
    }

    public function search_by_date_interval(Request $request)
    {
    	$startDate = Carbon::parse($request->initial_date)->format('Y-m-d');
        $endDate = Carbon::parse($request->final_date)->format('Y-m-d');

        $result = User::whereNull('email')->whereDate('created_at','>=',$startDate)
        ->whereDate('created_at','<=',$endDate)
        ->orderBy('id','desc')
        ->with('profile')
        ->get();

    	return $result;
    }

    public function search_deletes(Request $request)
    {
    	$result = User::onlyTrashed()->with('profile')->get();

    	return $result;
    }
}
