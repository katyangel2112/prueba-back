<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'id' => 1,
                    'first_name' => 'Usuario',
                    'last_name' => 'Actual',
                    'email' => 'user@gmail.com',
                    'password'  => bcrypt('123456'),
                    'description' => 'Usuario del Sistema',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
            ]
        );

        User::factory()->count(50)->create();
    }
}
