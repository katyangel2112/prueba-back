<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [
			'uses' => 'AuthController@login',
			'as'   => 'login'
]);

Route::group(['middleware' => 'auth:api'], function () {

	Route::post('/logout', [
				'uses' => 'AuthController@logout',
				'as'   => 'logout'
	]);

	Route::get('/index', [
				'uses' => 'UserController@index',
				'as'   => 'index'
	]);

	Route::post('/store', [
				'uses' => 'UserController@store',
				'as'   => 'store'
	]);

	Route::post('/editar', [
				'uses' => 'UserController@editar',
				'as'   => 'editar'
	]);

	Route::post('/update', [
				'uses' => 'UserController@update',
				'as'   => 'update'
	]);

	Route::post('/delete', [
				'uses' => 'UserController@delete',
				'as'   => 'delete'
	]);

	Route::post('/search_by_name', [
				'uses' => 'UserController@search_by_name',
				'as'   => 'search_by_name'
	]);

	Route::post('/search_by_id', [
				'uses' => 'UserController@search_by_id',
				'as'   => 'search_by_id'
	]);

	Route::post('/search_by_date', [
				'uses' => 'UserController@search_by_date',
				'as'   => 'search_by_date'
	]);

	Route::post('/search_by_date_interval', [
				'uses' => 'UserController@search_by_date_interval',
				'as'   => 'search_by_date_interval'
	]);

	Route::post('/search_deletes', [
				'uses' => 'UserController@search_deletes',
				'as'   => 'search_deletes'
	]);
});